# скрипты для аннотирования первой части датасета, ПОКА запускать во вторую очередь


import xml.etree.ElementTree as ET
from os import listdir
from os.path import isfile, join

from utils.consts import ANNOTATIONS_PATH_FIRST_PART as ANNOTATIONS_PATH
from utils.consts import DUMP_PATH_FIRST_PART as DUMP_PATH
from utils.consts import DATA_SOURCE

from utils.structure import CVAT_fields, dump


def get_num(str):
    return int(str[2:-4])

def write_first_part(starting_num, dump_path):

    cvat_tree = ET.parse(dump_path)
    cvat_root = cvat_tree.getroot()

    print('first dataset:')

    iter_count = len(listdir(ANNOTATIONS_PATH))
    progress = 0

    for pic_num, a_path in enumerate(sorted(listdir(ANNOTATIONS_PATH), key=get_num), starting_num):
        if isfile(join(ANNOTATIONS_PATH, a_path)) and a_path.endswith('.xml'):

            original_tree = ET.parse(join(ANNOTATIONS_PATH, a_path))
            original_root = original_tree.getroot()

            desc = CVAT_fields(pic_num, a_path, original_root)

            for kp_struct in original_root.iter('keypoint'):

                if int(kp_struct.get('visible')) == 1:
                    desc.points.append(
                        {
                            'label': kp_struct.get('name'),
                            'occluded': '0', # перекрытие, оставлю нулями
                            'source': DATA_SOURCE,
                            'points': kp_struct.get('x') + ',' + kp_struct.get('y'), 
                            'z_order': str(int(float(kp_struct.get('z'))))
                        }
                    )

            dump(cvat_tree, desc, dump_path)
        
        if int((pic_num - starting_num)/iter_count*100) - progress >= 10:
            print(f'progress: {progress}%')
            progress = int((pic_num - starting_num)/iter_count*100)
    print(f'progress: {progress}%')

    next_free_num = iter_count
    return next_free_num
