# скрипты для аннотирования второй части датасета


import xml.etree.ElementTree as ET
from os import listdir
from os.path import isfile, join

from utils.consts import ANNOTATIONS_PATH_SECOND_PART as ANNOTATIONS_PATH
from utils.consts import DUMP_PATH_SECOND_PART as DUMP_PATH
from utils.consts import IMAGES_PATH_SECOND_PART as IMAGES_PATH
from utils.consts import DATA_SOURCE

from utils.structure import CVAT_fields, dump

ANNOTATIONS_LIST = listdir(ANNOTATIONS_PATH)


def insert_num(path, num):
    ext = '.xml'
    return path[:-4] + '_' + str(num) + ext

def gen_next_numbered_annotation_name(annotations_dir, picture_name):
    # аннотация генерируется из имени изображения и _{очередной номер}

    target_annotation_base = picture_name.replace('.jpg', '')
    search_results_list = []

    for i in ANNOTATIONS_LIST:

        if i.split('_')[0] + '_' + i.split('_')[1] == target_annotation_base:
            search_results_list.append(i)

    for i in search_results_list:
        yield join(annotations_dir, i)

def write_second_part(starting_num, dump_path):
    cvat_tree = ET.parse(dump_path)
    cvat_root = cvat_tree.getroot()

    print('second dataset:')

    iter_count = len(listdir(IMAGES_PATH))
    progress = 0

    # пройдемся по каждой фотке и дампнем все соответствующие аннотации в один тэг
    for picture_number, picture_name in enumerate(listdir(IMAGES_PATH), starting_num):
        if isfile(join(IMAGES_PATH, picture_name)) and picture_name.endswith('.jpg'):

            # тут надо пройтись по всем файлам аннотации
            # и кейпоинты каждого пихнуть в decs
            is_first = True

            for numbered_annotation_path in gen_next_numbered_annotation_name(ANNOTATIONS_PATH, picture_name):
                # засунем в структуру инициализации первую существующую аннотацию
                # print(numbered_annotation_path)
                if is_first:
                    first_found_picture_annotation_path = numbered_annotation_path
                    original_tree = ET.parse(first_found_picture_annotation_path)
                    original_root = original_tree.getroot()
                    desc = CVAT_fields(picture_number, picture_name, original_root)
                    is_first = False

                current_annotation_root = ET.parse(numbered_annotation_path).getroot()

                for kp_struct in current_annotation_root.iter('keypoint'):

                    if int(kp_struct.get('visible')) == 1:
                        desc.points.append(
                            {
                                'label': kp_struct.get('name'),
                                'occluded': '0', # перекрытие, оставлю нулями
                                'source': DATA_SOURCE,
                                'points': kp_struct.get('x') + ',' + kp_struct.get('y'), 
                                'z_order': str(int(float(kp_struct.get('z'))))
                            }
                        )

            dump(cvat_tree, desc, dump_path)

        if int((picture_number - starting_num)/iter_count*100) - progress >= 10:
            print(f'progress: {progress}%')
            progress = int((picture_number - starting_num)/iter_count*100)
    print(f'progress: {progress}%')

    next_free_num = iter_count
    return next_free_num
