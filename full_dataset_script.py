from part1_script import write_first_part
from part2_script import write_second_part

if __name__ == '__main__':
    next_free_num = write_first_part(0, './dump_file.xml')
    write_second_part(next_free_num, './dump_file.xml')
