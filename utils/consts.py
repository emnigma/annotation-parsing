
ANNOTATIONS_PATH_FIRST_PART = './annotations_part1'
ANNOTATIONS_PATH_SECOND_PART = './annotations_part2'

IMAGES_PATH_FIRST_PART = './images_part1'
IMAGES_PATH_SECOND_PART = './images_part2'

DUMP_PATH_FIRST_PART = './dump_file_part1.xml'
DUMP_PATH_SECOND_PART = './dump_file_part2.xml'
DUMP_PATH_FULL = './dump_file.xml'

DATA_SOURCE = 'manual' # can't be blank and more, then 16 chars