# scripts for renaming annotation names(for some reason they are different for different datasets)

from os import listdir
from os.path import isfile, join, exists

ANNOTATIONS_PATH = './annotations_part1'

if __name__ =='__main__':
    part2_annotations_count = 200
    current_annotation_num = 0
    progress = 0

    for current_photo_path in listdir(ANNOTATIONS_PATH):
        if current_photo_path.split('/')[-1][0:2] == 'co':

            if not exists(join(ANNOTATIONS_PATH, current_photo_path)):
                throw("FUCK")

            with open(join(ANNOTATIONS_PATH, current_photo_path), 'r') as file:
                data = file.read()
            
            # should be 16 items
            data = data.replace('R_eye', 'R_Eye')
            data = data.replace('L_eye', 'L_Eye')
            data = data.replace('R_ear', 'R_Ear')
            data = data.replace('L_ear', 'L_Ear')
            data = data.replace('L_F_elbow', 'L_F_Elbow')
            data = data.replace('R_F_elbow', 'R_F_Elbow')
            data = data.replace('L_B_elbow', 'L_B_Elbow')
            data = data.replace('R_B_elbow', 'R_B_Elbow')
            data = data.replace('L_F_paw', 'L_F_Paw')
            data = data.replace('R_F_paw', 'R_F_Paw')
            data = data.replace('L_B_paw', 'L_B_Paw')
            data = data.replace('R_B_paw', 'R_B_Paw')
            data = data.replace('L_F_knee', 'L_F_Knee')
            data = data.replace('R_F_knee', 'R_F_Knee')
            data = data.replace('L_B_knee', 'L_B_Knee')
            data = data.replace('R_B_knee', 'R_B_Knee')
            data = data.replace('withers', 'Withers')

            with open(join(ANNOTATIONS_PATH, current_photo_path), 'w') as file:
                file.write(data)
            
            if current_annotation_num/part2_annotations_count*100 - progress > 1:
                print(f'progress: {int(progress)}%')
                progress = current_annotation_num/part2_annotations_count*100

            current_annotation_num += 1
