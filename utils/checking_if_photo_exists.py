# scripts for checking existing images

from os import listdir
from os.path import isfile, join, exists
from shutil import copyfile

GIVEN_IMAGES_DIR_PATH = './images_part1'
VOC11_IMAGES_DIR_PATH = './images_part2'
ANNOTATIONS_PART1_PATH = './annotations_part1'
ANNOTATIONS_PART2_PATH = './annotations_part2'

if __name__ =='__main__':
    annotations_count = len(listdir(ANNOTATIONS_PATH))

    progress = 0
    for current_photo_num, a_path in enumerate(listdir(ANNOTATIONS_PATH)): # переберет 200 лишних фоток, но кого волнует

        annotation_name = a_path.split('/')[-1][:-4]
        photo_name = annotation_name[:-2] + '.jpg'
        path_to_corresponding_photo = join(VOC11_IMAGES_DIR_PATH, photo_name)
        if exists(path_to_corresponding_photo):
            # copying image to images folder
            copy_dest = join(IMAGE_DIR, photo_name)
            copyfile(path_to_corresponding_photo, copy_dest)

        if current_photo_num/annotations_count*100 - progress > 1:
            print(f'progress: {int(progress)}%')
            progress = current_photo_num/annotations_count*100
