import xml.etree.ElementTree as ET
from os import listdir
from os.path import isfile, join


class CVAT_fields:

    def __init__(self, pic_num, annotation_path, root):
        self.image_id = str(pic_num)
        self.image_name = annotation_path
        self.image_width = str(int(float(root.find('visible_bounds').get('width'))))
        self.image_height = str(int(float(root.find('visible_bounds').get('height'))))
        self.points = [] # to store keypoints


def dump(cvat_tree, cvat_fields, dump_path):
    image_element = ET.Element(
        'image',
        {
            'id': cvat_fields.image_id,
            'name': cvat_fields.image_name,
            'width': cvat_fields.image_width,
            'height': cvat_fields.image_height,
        }
    )

    # inserting keypoints:
    for i in cvat_fields.points:
        ET.SubElement(
            image_element,
            'points',
            i
        )

    cvat_tree.getroot().append(
        image_element
    )
    cvat_tree.write(dump_path)
