

from os import listdir
from os.path import isfile, join, exists

import xml.etree.ElementTree as ET

from consts import ANNOTATIONS_PATH_FIRST_PART as ANNOTATIONS_DIR_1
from consts import ANNOTATIONS_PATH_SECOND_PART as ANNOTATIONS_DIR_2

from consts import DUMP_PATH_FIRST_PART as dp1
from consts import DUMP_PATH_SECOND_PART as dp2


# def check_generated():
if __name__ == '__main__':
    
    for xml_file in listdir(ANNOTATIONS_DIR_1):
        ex1 = False
        try:
            with open(join(ANNOTATIONS_DIR_1, xml_file)) as f:
                data = f.read()
                x = ET.fromstring(data)
        except Exception as e:
            print(f"{xml_file} is NOT well-formed! ALARM!{e}")
            ex1 = True
    if not ex1:
        print("there are no problem with 1t part")

    for xml_file in listdir(ANNOTATIONS_DIR_2):
        ex2 = False
        try:
            with open(join(ANNOTATIONS_DIR_2, xml_file)) as f:
                data = f.read()
                x = ET.fromstring(data)
        except Exception as e:
            print(f"{xml_file} is NOT well-formed! ALARM!{e}")
            ex2 = False
    if not ex2:
        print("there are no problem with 2d part")

    ex_dp1 = False
    try:
        with open(dp1) as f:
            data = f.read()
            x = ET.fromstring(data)
    except Exception as e:
        print(f"{xml_file} is NOT well-formed! ALARM!{e}")
        ex_dp1 = False
    if not ex_dp1:
        print("there are no problem with dp1")

    ex_dp2 = False
    try:
        with open(dp2) as f:
            data = f.read()
            x = ET.fromstring(data)
    except Exception as e:
        print(f"{xml_file} is NOT well-formed! ALARM!{e}")
        ex_dp2 = False
    if not ex_dp2:
        print("there are no problem with dp2")